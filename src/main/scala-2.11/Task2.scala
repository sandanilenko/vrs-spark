/**
  * Created by sandan on 28.05.16.
  */

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}

object Task2 {
//  На основе заданного текста подсчитать вхождение каждого слова. То
//  есть словарь в котором для каждого слова указывается сколько раз оно
//  входит в текст.
  def main(args: Array[String]) {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)
    val conf = new SparkConf().setAppName("Task2 Application").setMaster("local")
    val sc = new SparkContext(conf)
    val rdd = sc.textFile("text1.txt")

    val words = rdd.flatMap(line => line.split("[\\p{Punct}\\s]+")).sortBy(w => w)
    for(word <- words.countByValue) println(word)

    sc.stop
  }
}
