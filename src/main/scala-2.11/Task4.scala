/**
  * Created by sandan on 28.05.16.
  */

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.mllib.linalg.distributed.{CoordinateMatrix, MatrixEntry}

object Task4 {
//  Дана матрица чисел. Проверить является ли матрица симметричной
//  относительно главной диагонали.

  def main(args: Array[String]) {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)
    val conf = new SparkConf().setAppName("Task4 Application").setMaster("local")
    val sc = new SparkContext(conf)
    val entries = sc.parallelize(Seq(
      (0, 0, 3.0), (0, 1, 6.0), (0, 2, 6.0),
      (1, 0, 6.0), (1, 1, 2.0), (1, 2, 4.0),
      (2, 0, 6.0), (2, 1, 4.0), (2, 2, 4.0))
    ).map{case (i, j, v) => MatrixEntry(i, j, v)}
    val s = 3.0 // size of square matrix
    val uniqueCount = s + (s * s - s) / 2.0 // count of elements from high and main diagonals
    val isSymmetric2 = entries.map(e => if(e.i > e.j) MatrixEntry(e.j, e.i, e.value) else e).distinct.count == uniqueCount
    println("4: "); println("Is symmetric2: " + isSymmetric2)
    sc.stop
  }
}
