/**
  * Created by sandan on 28.05.16.
  */

import org.apache.log4j.{Level, Logger}
import org.apache.spark.mllib.linalg.distributed.MatrixEntry
import org.apache.spark.{SparkConf, SparkContext}

object Task5 {
//  Написать программу перемножения двух матриц.
  def main(args: Array[String]) {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)
    val conf = new SparkConf().setAppName("Task5 Application").setMaster("local")
    val sc = new SparkContext(conf)
    val A = sc.parallelize(Seq(
      (0, 0, 1), (0, 1, 2),
      (1, 0, 3), (1, 1, 4))
    ).map{case (i, j, v) => MatrixEntry(i, j, v)}
    val B = sc.parallelize(Seq(
      (0, 0, 5), (0, 1, 6),
      (1, 0, 7), (1, 1, 8))
    ).map{case (i, j, v) => MatrixEntry(i, j, v)}

    val mul = A
      .cartesian(B)
      .filter(e => e._1.j == e._2.i)
      .map(e => ((e._1.i, e._2.j), e._1.value * e._2.value))
      .groupBy(e => e._1)
      .map(e => e._2.reduce((a1, a2) => ((a1._1._1, a1._1._2), a1._2 + a2._2)))
      .map(e => MatrixEntry(e._1._1, e._1._2, e._2))
    for(r <- mul) println(r)
    sc.stop
  }
}
