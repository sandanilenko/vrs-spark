/**
  * Created by sandan on 28.05.16.
  */

import org.apache.log4j.{Level, Logger}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object Task3 {
//  Даны два разных текста. Сформировать 3 множества: пересечение
//  словарей заданных текстов, множество слов, входящих только в первый
//  текст, множество слов, входящих только во второй текст.

  def sortedWords(text: RDD[String]) = text.flatMap(line => line.split("[\\p{Punct}\\s]+")).sortBy(w => w)

  def main(args: Array[String]) {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)
    val conf = new SparkConf().setAppName("Task3 Application").setMaster("local")
    val sc = new SparkContext(conf)
    val rdd1 = sc.textFile("text1.txt")
    val rdd2 = sc.textFile("text2.txt")

    val words1 = sortedWords(rdd1)
    val words2 = sortedWords(rdd2)

    println("Intersection: ----------------------") ; for(s <- words1.intersection(words2)) println(s)
    println("Only 1st: ----------------------") ; for(s <- words1.subtract(words2)) println(s)
    println("Only 2nd: ----------------------") ; for(s <- words2.subtract(words1)) println(s)

    sc.stop
  }
}
