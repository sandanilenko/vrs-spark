/**
  * Created by sandan on 28.05.16.
  */

import org.apache.log4j.{Level, Logger}
import org.apache.spark.mllib.linalg.distributed.MatrixEntry
import org.apache.spark.{SparkConf, SparkContext}

object Task6 {
//  Дана матрица a. Сформировать новую матрицу где каждый элемент a ij
//  заменен на значение выражения a ij = a i+2,j + a i-2,j + a i,j+2 + a i,j-2 + 4a i,j
  def main(args: Array[String]) {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)
    val conf = new SparkConf().setAppName("Task6 Application").setMaster("local")
    val sc = new SparkContext(conf)
    val a = sc.parallelize(Seq(
      (0, 0, 1), (0, 1, 2), (0, 2, 3), (0, 3, 4), (0, 4, 5), (0, 5, 6),
      (1, 0, 1), (1, 1, 2), (1, 2, 3), (1, 3, 4), (1, 4, 5), (1, 5, 6),
      (2, 0, 1), (2, 1, 2), (2, 2, 3), (2, 3, 4), (2, 4, 5), (2, 5, 6),
      (3, 0, 1), (3, 1, 2), (3, 2, 3), (3, 3, 4), (3, 4, 5), (3, 5, 6),
      (4, 0, 1), (4, 1, 2), (4, 2, 3), (4, 3, 4), (4, 4, 5), (4, 5, 6),
      (5, 0, 1), (5, 1, 2), (5, 2, 3), (5, 3, 4), (5, 4, 5), (5, 5, 6))
    ).map { case (i, j, v) => MatrixEntry(i, j, v) }

    val aSize: Int = 6

    val changed = a
      .filter(e => e.i >= 2 && e.j >= 2 && e.i < aSize - 2 && e.j < aSize - 2)
      .cartesian(a)
      .map(e => if(e._2.i == e._1.i && e._2.j == e._1.j) (e._1, MatrixEntry(e._2.i, e._2.j, e._2.value * 4)) else e)
      .filter(e =>
        e._2.j == e._1.j && (e._2.i == e._1.i + 2 || e._2.i == e._1.i - 2) ||
          e._2.i == e._1.i && (e._2.j == e._1.j + 2 || e._2.j == e._1.j - 2 || e._2.j == e._1.j))
      .groupBy(e => e._1)
      .map(e => e._2.reduce((a1, a2) => (e._1, MatrixEntry(0, 0, a1._2.value + a2._2.value))))
      .map(e => MatrixEntry(e._1.i, e._1.j, e._2.value))

    val aNew = a
      .filter(e => e.i < 2 || e.j < 2 || e.i >= aSize - 2 || e.j >= aSize - 2)
      .union(changed)
      .sortBy(e => e.j)
      .sortBy(e => e.i)

    for(r <- aNew) println(r)
    sc.stop
  }
}
