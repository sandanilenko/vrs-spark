/**
  * Created by sandan on 28.05.16.
  */

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}

object Task1 {
//  На основе заданного текста в файле составить словарь — то есть список
//  всех уникальных слов, используемых в тексте в алфавитном порядке.
//  Знаки препинания игнорировать.

  def main(args: Array[String]) {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)
    val conf = new SparkConf().setAppName("Task1 Application").setMaster("local")
    val sc = new SparkContext(conf)
    val rdd = sc.textFile("text1.txt")

    val words = rdd.flatMap(line => line.split("[\\p{Punct}\\s]+"))
    for(word <- words.distinct.sortBy(w => w)) println(word)

    sc.stop
  }
}
